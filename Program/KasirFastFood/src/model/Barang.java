/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

/**
 *
 * @author NatanHariPamungkas
 */
public class Barang {
    String namaBarang, kodeBarang;
    int stok;
    float harga;
    
    public Barang(String namaBarang, String kodeBarang, int stok, float harga){
        this.namaBarang=namaBarang;
        this.kodeBarang=kodeBarang;
        this.stok=stok;
        this.harga=harga;
    }
    
    public Barang(String kodeBarang, int stok){
        this.kodeBarang=kodeBarang;
        this.stok=stok;
    }
    
    public Barang(){
        
    }

    public String getNamaBarang() {
        return namaBarang;
    }

    public void setNamaBarang(String namaBarang) {
        this.namaBarang = namaBarang;
    }

    public String getKodeBarang() {
        return kodeBarang;
    }

    public void setKodeBarang(String kodeBarang) {
        this.kodeBarang = kodeBarang;
    }

    public int getStok() {
        return stok;
    }

    public void setStok(int stok) {
        this.stok = stok;
    }

    public float getHarga() {
        return harga;
    }

    public void setHarga(float harga) {
        this.harga = harga;
    }
}
