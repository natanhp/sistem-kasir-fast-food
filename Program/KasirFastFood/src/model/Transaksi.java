/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

/**
 *
 * @author NatanHariPamungkas
 */
public abstract class Transaksi {
    protected float totalHarga;
    protected final float ppn = 0.1f;
    public Transaksi(){
    }

    public float getTotalHarga() {
        return totalHarga;
    }

    public void setTotalHarga(float totalHarga) {
        this.totalHarga = totalHarga;
    }
    
    public abstract float hitungTotalHarga();
}
