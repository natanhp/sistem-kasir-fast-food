/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author NatanHariPamungkas
 */
public class Karyawan {
    private String nama, status, hash;
    private double lamaKerja;
    private double gaji;
    private List<Barang> barang;
    private Barang dataBarang;
    private Delivery dataDelivery;
    private Pemesanan dataPemesanan;
    public static String user;
    public Karyawan(){
        dataPemesanan = new Pemesanan();
        barang = new ArrayList<>();
        dataDelivery = new Delivery();
    }
    
    public Karyawan(String kodeBarang, int stok){
        dataBarang = new Barang(kodeBarang, stok);
    }
    
    public Karyawan(String nama, String status, float lamaKerja, float gaji){
        this.nama=nama;
        this.status = status;
        this.lamaKerja = lamaKerja;
        this.gaji=gaji;
    }

    public String getNama() {
        return nama;
    }

    public String getStatus() {
        return status;
    }

    public double getLamaKerja() {
        return lamaKerja;
    }

    public double getGaji() {
        return gaji;
    }

    public void setNama(String nama) {
        this.nama = nama;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public void setLamaKerja(double lamaKerja) {
        this.lamaKerja = lamaKerja;
    }

    public void setGaji(double gaji) {
        this.gaji = gaji;
    }
    
    public double hitungGaji(){
        return lamaKerja*gaji;
    }
    
    public void setHash(String hash){
        this.hash = hash;
    }
    
    public String getHash(){
        return hash;
    }
    
    public void addDelivery(Delivery dataDelivery ){
        this.dataDelivery = dataDelivery;
    }
    
    public Delivery getDelivery(){
        return dataDelivery;
    }
    
    public void addBarang(List<Barang> b){
        barang = b;
    }
    
    public List<Barang> getBarang(){
        return barang;
    }
    
    public int gerMaxData(){
        return barang.size();
    }
    
    public String hashPassword(String password) throws NoSuchAlgorithmException{
        MessageDigest md = MessageDigest.getInstance("MD5");
        byte[] hashInBytes = md.digest(password.getBytes(StandardCharsets.UTF_8));
        StringBuilder sb = new StringBuilder();
        for (byte b : hashInBytes) {
            sb.append(String.format("%02x", b));
        }
        return sb.toString();
    }
    
    
    public String getKodeBarang(){
        return dataBarang.getKodeBarang();
    }
    
    public int getStokBarang(){
        return dataBarang.getStok();
    }
    
    public void setPemesanan(Pemesanan dataPemesanan){
        this.dataPemesanan = dataPemesanan;
    }
    
    public Pemesanan getPemesanan(){
        return dataPemesanan;
    }
}
