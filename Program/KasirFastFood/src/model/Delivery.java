/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

/**
 *
 * @author NatanHariPamungkas
 */
public class Delivery implements ITransaksi{
    private int jumlahItem;
    private double subTotal;
    private double harga;
    private String kodeBarang;
    
    public Delivery(int jumlahItem, float harga, String kodeBarang){
        this.jumlahItem = jumlahItem;
        this.harga = harga;
        this.kodeBarang = kodeBarang;
    }
    
    public Delivery(){
        
    }
    
    public int getJumlahItem() {
        return jumlahItem;
    }

    public void setJumlahItem(int jumlahItem) {
        this.jumlahItem = jumlahItem;
    }
    
    public void setSubTotal(double subTotal){
        this.subTotal = subTotal;
    }
 
    public double hitungSubTotal(){
        return subTotal;
    }
}
