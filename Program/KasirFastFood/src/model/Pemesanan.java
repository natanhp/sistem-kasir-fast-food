/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;
/**
 *
 * @author NatanHariPamungkas
 */
public class Pemesanan implements ITransaksi{
    private int jumlahItem;
    private double total;
    private String statusBarang;
    
    public Pemesanan(int jumlah, double total){
        jumlahItem = jumlah;
        this.total = total;
    }

    public Pemesanan() {
    }

    public int getJumlahItem() {
        return jumlahItem;
    }

    public void setJumlahItem(int jumlahItem) {
        this.jumlahItem = jumlahItem;
    }

    public void setTotal(double total) {
        this.total = total;
    }

    public void setStatusBarang(String statusBarang) {
        this.statusBarang = statusBarang;
    }

    public String getStatusBarang() {
        return statusBarang;
    }

    @Override
    public double hitungSubTotal() {
        return total;
    }
}
