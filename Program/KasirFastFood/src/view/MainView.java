/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package view;
import model.Karyawan;
import control.BarangControl;
import java.util.ArrayList;
import java.util.List;
import java.util.Vector;
import model.Barang;
import model.Pemesanan;
import model.Delivery;
import control.KaryawanControl;
import control.PemesananControl;
import control.DeliveryControl;
import java.security.NoSuchAlgorithmException;
import java.util.logging.Level;
import java.util.logging.Logger;
import exception.PasswordFormatException;
import java.util.regex.Pattern;
import javax.swing.JOptionPane;
import exception.NamaFormatException;
import exception.StatusPegawaiException;
import exception.TransaksiKosongException;
import exception.JenisPemesananException;
import javax.swing.table.DefaultTableModel;

/**
 *
 * @author NatanHariPamungkas
 */
public class MainView extends javax.swing.JFrame {

    /**
     * Creates new form MainView
     */
    DefaultTableModel tabelModel, tblModelKaryawan, tblModelTransaksi;
    BarangControl barangControl = new BarangControl();
    KaryawanControl karyawanControl = new KaryawanControl();
    PemesananControl pemesananControl = new PemesananControl();
    DeliveryControl deliveryControl = new DeliveryControl();
    double timeStart;
    public MainView() {
        initComponents();
        tabelModel = (DefaultTableModel) tblBarang.getModel();
        tblModelKaryawan = (DefaultTableModel) tblKaryawan.getModel();
        tblModelTransaksi = (DefaultTableModel) tblTransaksi.getModel();
        setEnTab();
        showTable();
        timeStart = System.currentTimeMillis();
        txtTotal.setEditable(false);
    }
    
    public void setEnTab(){
        setEnablePegawai(false);
        if(Karyawan.user.equals("root")){
            setEnablePegawai(true);
        }
    }
    
    public void passwordFormatException(String nama, String password) throws PasswordFormatException{
        boolean regex = Pattern.matches("^(?=.*[0-9])(?=.*[a-z])(?=.*[A-Z])(?=.*[@#$%^&+=])(?=\\S+$).{8,}$", password);
        boolean banding = nama.equalsIgnoreCase(password);
        if((!regex) && banding){
            throw new PasswordFormatException();
        }
    }
    
    public void namaFormatException(String nama) throws NamaFormatException{
        boolean regex = Pattern.matches("^[a-z]{1,}$", nama);
        if(!regex){
            throw new NamaFormatException();
        }
    }
    
    public void statusPegawaiException(boolean status) throws StatusPegawaiException{
        if(!status){
            throw new StatusPegawaiException();
        }
    }
    
    public void transaksiKosongException() throws TransaksiKosongException{
        if(tblTransaksi.getRowCount()==0){
            throw new TransaksiKosongException();
        }
    }
    
    public void jenisPemesananException(boolean status) throws JenisPemesananException{
        if(!status){
            throw new JenisPemesananException();
        }
    }
    
    public void setEnablePegawai(boolean val){
        txtNama.setEnabled(val);
        pwdPassword.setEnabled(val);
        btnSimpan.setEnabled(val);
        jrButtonPgwTetap.setEnabled(val);
        jrButtonMagang.setEnabled(val);
    }
    
    public void showTable(){
        List<Barang> barang = new ArrayList();
        barang = barangControl.loadBarang();
        tabelModel.getDataVector().removeAllElements();
        tabelModel.fireTableDataChanged();
        for(int i=0; i<barang.size(); i++){
            Object[] rowData = new Object[3];
            rowData[0] = barang.get(i).getNamaBarang();
            rowData[1] = barang.get(i).getKodeBarang();
            rowData[2] = barang.get(i).getStok();
            tabelModel.addRow(rowData);
        }
        List<Karyawan> karyawan = new ArrayList();
        if(Karyawan.user.equals("root")){
            karyawan = karyawanControl.showKaryawan();
        }else{
            karyawan = karyawanControl.showKaryawanTertentu(Karyawan.user);
        }
        
        tblModelKaryawan.getDataVector().removeAllElements();
        tblModelKaryawan.fireTableDataChanged();
        for(int i=0; i<karyawan.size(); i++){
            Object[] rowData = new Object[4];
            rowData[0] = karyawan.get(i).getNama();
            rowData[1] = karyawan.get(i).getStatus();
            rowData[2] = karyawan.get(i).getGaji();
            rowData[3] = karyawan.get(i).getLamaKerja();
            tblModelKaryawan.addRow(rowData);
        }
    }
    
    public int cariBarangTransaksi(String kodeBarang){
        int baris = tblTransaksi.getRowCount();
        for(int i=0; i<baris; i++){
            String kode=tblModelTransaksi.getValueAt(i, 1).toString();
            if(kode.equals(kodeBarang)){
                return i;
            }
        }
        return -1;
    }
    
    public int hitungJumlahBarang(){
        int baris = tblTransaksi.getRowCount();
        int jumlahBarang=0;
        for(int i=0; i<baris; i++){
            int nilai=Integer.parseInt(tblModelTransaksi.getValueAt(i, 2).toString());
            jumlahBarang+=nilai;
        }
        return jumlahBarang;
    }
    
    public double jumlahHarga(){
        int baris = tblTransaksi.getRowCount();
        double hargaBarang=0;
        for(int i=0; i<baris; i++){
            double nilai=Double.parseDouble(tblModelTransaksi.getValueAt(i, 3).toString());
            hargaBarang+=nilai;
        }
        return hargaBarang;
    }
    
    public int cariBarang(String kodeBarang){
        int baris = tblBarang.getRowCount();
        for(int i=0; i<baris; i++){
            String kode=tabelModel.getValueAt(i, 1).toString();
            if(kode.equals(kodeBarang)){
                return i;
            }
        }
        return -1;
    }
    
    public void updateTblBarang(String kodeBarang){
        tabelModel.fireTableDataChanged();
        int index = cariBarang(kodeBarang);
        int jumlah = Integer.parseInt(tabelModel.getValueAt(index, 2).toString())-1;
        barangControl.restokBarang(jumlah, kodeBarang);
        showTable();
    }
    
    public void updateTblTransaksi(int indexBarang, int jumlah){
        List<Barang> barang = new ArrayList();
        barang = barangControl.loadBarang();
//        tblModelTransaksi.getDataVector().removeAllElements();
        tblModelTransaksi.fireTableDataChanged();
        int i=indexBarang;
        Object[] rowData = new Object[4];
        rowData[0] = barang.get(i).getNamaBarang();
        rowData[1] = barang.get(i).getKodeBarang();
        rowData[2] = jumlah;
        rowData[3] = barang.get(i).getHarga() * Float.parseFloat(rowData[2].toString());
        tblModelTransaksi.addRow(rowData);
        updateTblBarang(rowData[1].toString());
    }
    
    public boolean checkStokBarang(){
        int rowIndex = tblBarang.getRowCount();
        for(int i=0; i<rowIndex; i++){
            int check= Integer.parseInt(tabelModel.getValueAt(i, 2).toString());
            if(check<=0){
                return true;
            }
        }
        return false;
    }
    
    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        btnGroupPegawai = new javax.swing.ButtonGroup();
        btnGroupPemesanan = new javax.swing.ButtonGroup();
        jPanel1 = new javax.swing.JPanel();
        tabMenu = new javax.swing.JTabbedPane();
        panelPegawai = new javax.swing.JPanel();
        jScrollPane1 = new javax.swing.JScrollPane();
        tblKaryawan = new javax.swing.JTable();
        jLabel1 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        txtNama = new javax.swing.JTextField();
        jLabel3 = new javax.swing.JLabel();
        pwdPassword = new javax.swing.JPasswordField();
        jrButtonPgwTetap = new javax.swing.JRadioButton();
        jrButtonMagang = new javax.swing.JRadioButton();
        btnSimpan = new javax.swing.JButton();
        jPanel2 = new javax.swing.JPanel();
        btnCoke = new javax.swing.JButton();
        btnBurger = new javax.swing.JButton();
        jLabel4 = new javax.swing.JLabel();
        jLabel5 = new javax.swing.JLabel();
        btnPaket1 = new javax.swing.JButton();
        btnPaket2 = new javax.swing.JButton();
        jScrollPane2 = new javax.swing.JScrollPane();
        tblTransaksi = new javax.swing.JTable();
        jLabel6 = new javax.swing.JLabel();
        jScrollPane4 = new javax.swing.JScrollPane();
        tblBarang = new javax.swing.JTable();
        btnBatal = new javax.swing.JButton();
        btnHapus = new javax.swing.JButton();
        btnProses = new javax.swing.JButton();
        btnRestok = new javax.swing.JButton();
        btnLogOut = new javax.swing.JButton();
        btnAyamGoreng = new javax.swing.JButton();
        btnFrenchFries = new javax.swing.JButton();
        btnPepsi = new javax.swing.JButton();
        btnMangoFloat = new javax.swing.JButton();
        jrButtonPemesanan = new javax.swing.JRadioButton();
        jrButtonDelivery = new javax.swing.JRadioButton();
        txtTotal = new javax.swing.JTextField();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        jPanel1.setBackground(new java.awt.Color(0, 153, 153));

        tblKaryawan.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "Nama", "Status", "Gaji", "Lama Shift"
            }
        ) {
            Class[] types = new Class [] {
                java.lang.String.class, java.lang.String.class, java.lang.Float.class, java.lang.Float.class
            };
            boolean[] canEdit = new boolean [] {
                false, false, false, false
            };

            public Class getColumnClass(int columnIndex) {
                return types [columnIndex];
            }

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        jScrollPane1.setViewportView(tblKaryawan);

        jLabel1.setText("Input Pegawai");

        jLabel2.setText("Nama");

        txtNama.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtNamaActionPerformed(evt);
            }
        });

        jLabel3.setText("Password");

        btnGroupPegawai.add(jrButtonPgwTetap);
        jrButtonPgwTetap.setText("Pegawai Tetap");
        jrButtonPgwTetap.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jrButtonPgwTetapActionPerformed(evt);
            }
        });

        btnGroupPegawai.add(jrButtonMagang);
        jrButtonMagang.setText("Magang");
        jrButtonMagang.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jrButtonMagangActionPerformed(evt);
            }
        });

        btnSimpan.setText("Simpan");
        btnSimpan.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnSimpanActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout panelPegawaiLayout = new javax.swing.GroupLayout(panelPegawai);
        panelPegawai.setLayout(panelPegawaiLayout);
        panelPegawaiLayout.setHorizontalGroup(
            panelPegawaiLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jScrollPane1)
            .addGroup(panelPegawaiLayout.createSequentialGroup()
                .addGroup(panelPegawaiLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(panelPegawaiLayout.createSequentialGroup()
                        .addGap(460, 460, 460)
                        .addComponent(jLabel1))
                    .addGroup(panelPegawaiLayout.createSequentialGroup()
                        .addGap(305, 305, 305)
                        .addGroup(panelPegawaiLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(panelPegawaiLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                .addGroup(panelPegawaiLayout.createSequentialGroup()
                                    .addComponent(jLabel2)
                                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                    .addComponent(txtNama, javax.swing.GroupLayout.PREFERRED_SIZE, 264, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addGroup(panelPegawaiLayout.createSequentialGroup()
                                    .addComponent(jLabel3)
                                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                    .addGroup(panelPegawaiLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                        .addGroup(panelPegawaiLayout.createSequentialGroup()
                                            .addComponent(jrButtonPgwTetap)
                                            .addGap(18, 18, 18)
                                            .addComponent(jrButtonMagang))
                                        .addComponent(pwdPassword, javax.swing.GroupLayout.PREFERRED_SIZE, 264, javax.swing.GroupLayout.PREFERRED_SIZE))))
                            .addGroup(panelPegawaiLayout.createSequentialGroup()
                                .addGap(152, 152, 152)
                                .addComponent(btnSimpan)))))
                .addContainerGap(387, Short.MAX_VALUE))
        );
        panelPegawaiLayout.setVerticalGroup(
            panelPegawaiLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panelPegawaiLayout.createSequentialGroup()
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 124, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(jLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addGroup(panelPegawaiLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel2)
                    .addComponent(txtNama, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(panelPegawaiLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel3)
                    .addComponent(pwdPassword, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(panelPegawaiLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jrButtonPgwTetap)
                    .addComponent(jrButtonMagang))
                .addGap(18, 18, 18)
                .addComponent(btnSimpan)
                .addGap(0, 370, Short.MAX_VALUE))
        );

        tabMenu.addTab("Pegawai", panelPegawai);

        btnCoke.setText("Icon Coke");
        btnCoke.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnCokeActionPerformed(evt);
            }
        });

        btnBurger.setText("Icon Burger");
        btnBurger.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnBurgerActionPerformed(evt);
            }
        });

        jLabel4.setText("Satuan");

        jLabel5.setText("Paketan");

        btnPaket1.setText("Icon Paket 1");
        btnPaket1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnPaket1ActionPerformed(evt);
            }
        });

        btnPaket2.setText("Icon Paket 2");
        btnPaket2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnPaket2ActionPerformed(evt);
            }
        });

        tblTransaksi.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "Nama", "Kode Barang", "Jumlah", "Harga"
            }
        ) {
            Class[] types = new Class [] {
                java.lang.String.class, java.lang.String.class, java.lang.Integer.class, java.lang.Float.class
            };
            boolean[] canEdit = new boolean [] {
                false, false, false, false
            };

            public Class getColumnClass(int columnIndex) {
                return types [columnIndex];
            }

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        jScrollPane2.setViewportView(tblTransaksi);

        jLabel6.setText("Restok");

        tblBarang.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "Nama", "Kode Barang", "Stok"
            }
        ) {
            Class[] types = new Class [] {
                java.lang.String.class, java.lang.String.class, java.lang.Integer.class
            };
            boolean[] canEdit = new boolean [] {
                false, false, true
            };

            public Class getColumnClass(int columnIndex) {
                return types [columnIndex];
            }

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        jScrollPane4.setViewportView(tblBarang);

        btnBatal.setText("Batal");
        btnBatal.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnBatalActionPerformed(evt);
            }
        });

        btnHapus.setText("Hapus");
        btnHapus.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnHapusActionPerformed(evt);
            }
        });

        btnProses.setText("Proses");
        btnProses.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnProsesActionPerformed(evt);
            }
        });

        btnRestok.setText("Restok");
        btnRestok.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnRestokActionPerformed(evt);
            }
        });

        btnLogOut.setText("LogOut");
        btnLogOut.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnLogOutActionPerformed(evt);
            }
        });

        btnAyamGoreng.setText("Ayam Goren");
        btnAyamGoreng.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnAyamGorengActionPerformed(evt);
            }
        });

        btnFrenchFries.setText("Icon French Fries");
        btnFrenchFries.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnFrenchFriesActionPerformed(evt);
            }
        });

        btnPepsi.setText("Pepsi");
        btnPepsi.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnPepsiActionPerformed(evt);
            }
        });

        btnMangoFloat.setText("Mango Float");
        btnMangoFloat.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnMangoFloatActionPerformed(evt);
            }
        });

        btnGroupPemesanan.add(jrButtonPemesanan);
        jrButtonPemesanan.setText("Pemesanan");

        btnGroupPemesanan.add(jrButtonDelivery);
        jrButtonDelivery.setText("Delivery");

        txtTotal.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtTotalActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addGap(180, 180, 180)
                .addComponent(btnRestok)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(btnLogOut)
                .addGap(23, 23, 23))
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel2Layout.createSequentialGroup()
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addContainerGap()
                        .addComponent(jScrollPane4, javax.swing.GroupLayout.PREFERRED_SIZE, 469, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addGap(208, 208, 208)
                        .addComponent(jLabel6))
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addGap(47, 47, 47)
                        .addComponent(btnBatal)
                        .addGap(67, 67, 67)
                        .addComponent(btnHapus)
                        .addGap(61, 61, 61)
                        .addComponent(btnProses))
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addContainerGap()
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(txtTotal, javax.swing.GroupLayout.PREFERRED_SIZE, 441, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jScrollPane2, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))))
                .addGap(18, 18, 18)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addGap(0, 9, Short.MAX_VALUE)
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addGroup(jPanel2Layout.createSequentialGroup()
                                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addGroup(jPanel2Layout.createSequentialGroup()
                                        .addGap(77, 77, 77)
                                        .addComponent(jLabel4))
                                    .addGroup(jPanel2Layout.createSequentialGroup()
                                        .addComponent(btnBurger, javax.swing.GroupLayout.PREFERRED_SIZE, 104, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addGap(18, 18, 18)
                                        .addComponent(btnCoke, javax.swing.GroupLayout.PREFERRED_SIZE, 104, javax.swing.GroupLayout.PREFERRED_SIZE)))
                                .addGap(26, 26, 26)
                                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addGroup(jPanel2Layout.createSequentialGroup()
                                        .addComponent(btnPaket1, javax.swing.GroupLayout.PREFERRED_SIZE, 104, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                        .addComponent(btnPaket2, javax.swing.GroupLayout.PREFERRED_SIZE, 104, javax.swing.GroupLayout.PREFERRED_SIZE))
                                    .addGroup(jPanel2Layout.createSequentialGroup()
                                        .addGap(84, 84, 84)
                                        .addComponent(jLabel5)))
                                .addGap(43, 43, 43))
                            .addGroup(jPanel2Layout.createSequentialGroup()
                                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                    .addComponent(btnAyamGoreng, javax.swing.GroupLayout.PREFERRED_SIZE, 104, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(btnFrenchFries, javax.swing.GroupLayout.PREFERRED_SIZE, 104, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addGap(18, 18, 18)
                                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(btnPepsi, javax.swing.GroupLayout.PREFERRED_SIZE, 104, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(btnMangoFloat, javax.swing.GroupLayout.PREFERRED_SIZE, 104, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addGap(289, 289, 289))))
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addComponent(jrButtonPemesanan)
                        .addGap(18, 18, 18)
                        .addComponent(jrButtonDelivery)
                        .addGap(0, 0, Short.MAX_VALUE))))
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addGap(30, 30, 30)
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel4)
                            .addComponent(jLabel5))
                        .addGap(33, 33, 33)
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(btnBurger, javax.swing.GroupLayout.PREFERRED_SIZE, 57, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(btnCoke, javax.swing.GroupLayout.PREFERRED_SIZE, 57, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(btnPaket1, javax.swing.GroupLayout.PREFERRED_SIZE, 57, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(btnPaket2, javax.swing.GroupLayout.PREFERRED_SIZE, 57, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(btnFrenchFries, javax.swing.GroupLayout.PREFERRED_SIZE, 57, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(btnPepsi, javax.swing.GroupLayout.PREFERRED_SIZE, 57, javax.swing.GroupLayout.PREFERRED_SIZE)))
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addContainerGap()
                        .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, 201, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(btnBatal, javax.swing.GroupLayout.DEFAULT_SIZE, 27, Short.MAX_VALUE)
                            .addComponent(btnHapus)
                            .addComponent(btnProses))
                        .addGap(30, 30, 30)
                        .addComponent(txtTotal, javax.swing.GroupLayout.PREFERRED_SIZE, 125, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(46, 46, 46))
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(btnAyamGoreng, javax.swing.GroupLayout.PREFERRED_SIZE, 57, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(btnMangoFloat, javax.swing.GroupLayout.PREFERRED_SIZE, 57, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
                .addComponent(jLabel6)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addComponent(jScrollPane4, javax.swing.GroupLayout.PREFERRED_SIZE, 174, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel2Layout.createSequentialGroup()
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jrButtonPemesanan)
                            .addComponent(jrButtonDelivery))
                        .addGap(43, 43, 43)))
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btnRestok)
                    .addComponent(btnLogOut))
                .addContainerGap())
        );

        tabMenu.addTab("Transasksi", jPanel2);

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(tabMenu, javax.swing.GroupLayout.PREFERRED_SIZE, 1028, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(tabMenu, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void jrButtonMagangActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jrButtonMagangActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_jrButtonMagangActionPerformed

    private void btnBurgerActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnBurgerActionPerformed
        // TODO add your handling code here:
        txtTotal.setText("");
        String kodeBarang="BGR-1";
        int kondisi = cariBarangTransaksi(kodeBarang);
        if(kondisi==-1){
            updateTblTransaksi(0, 1);
        }else{
            int nilai = Integer.parseInt(tblTransaksi.getValueAt(kondisi, 2).toString())+1;
            tblModelTransaksi.removeRow(kondisi);
            updateTblTransaksi(0, nilai);
        }
    }//GEN-LAST:event_btnBurgerActionPerformed

    private void txtNamaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtNamaActionPerformed
        // TODO add your handling code here:  
    }//GEN-LAST:event_txtNamaActionPerformed

    private void jrButtonPgwTetapActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jrButtonPgwTetapActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_jrButtonPgwTetapActionPerformed

    private void btnRestokActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnRestokActionPerformed
        // TODO add your handling code here:
        int index = tblBarang.getRowCount();
        for(int i=0; i<index; i++){
            String kodeBarang =tabelModel.getValueAt(i, 1).toString();
            int stokBarang = Integer.parseInt(tabelModel.getValueAt(i, 2).toString());
            System.out.println(stokBarang);
            barangControl.restokBarang(stokBarang, kodeBarang);
        }
        showTable();
    }//GEN-LAST:event_btnRestokActionPerformed

    private void btnSimpanActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnSimpanActionPerformed
        // TODO add your handling code here:
        try {    
            Karyawan karyawan = new Karyawan();
            karyawan.setNama(txtNama.getText());
            if(jrButtonPgwTetap.isSelected()==true){
                karyawan.setStatus("Pegawai Tetap");
            }else if(jrButtonMagang.isSelected()==true){
                karyawan.setStatus("Magang");
            }
            karyawan.setGaji(0);
            karyawan.setLamaKerja(0);
            namaFormatException(txtNama.getText());
            passwordFormatException(txtNama.getText(), pwdPassword.getText());
            statusPegawaiException(jrButtonPgwTetap.isSelected() || jrButtonMagang.isSelected());
            karyawanControl.inputKaryawan(karyawan, pwdPassword.getText());
            txtNama.setText("");
            pwdPassword.setText("");
        } catch (NoSuchAlgorithmException ex) {
            Logger.getLogger(MainView.class.getName()).log(Level.SEVERE, null, ex);
        }catch(PasswordFormatException e){
            JOptionPane.showMessageDialog(this, e.message(), "Error", JOptionPane.ERROR_MESSAGE);
        }catch(NamaFormatException e1){
            JOptionPane.showMessageDialog(this, e1.message(), "Error", JOptionPane.ERROR_MESSAGE);
        }catch(StatusPegawaiException e2){
            JOptionPane.showMessageDialog(this, e2.message(), "Error", JOptionPane.ERROR_MESSAGE);
        }
        showTable();
    }//GEN-LAST:event_btnSimpanActionPerformed

    private void btnLogOutActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnLogOutActionPerformed
        // TODO add your handling code here:
        if(!Karyawan.user.equals("root")){
            double time = (System.currentTimeMillis()-timeStart)/1000.0;
            double toHour = time/3600.0;
            karyawanControl.updateTimeShiftKaryawan(toHour, Karyawan.user);
            Object[] data = new Object[2];
            data = karyawanControl.updateGajiKaryawan(Karyawan.user);
            float gaji = 0;
            System.out.println(data[0]);
            if(data[0].toString().equals("Pegawai Tetap")){
                gaji=10000;
            }else if(data[0].toString().equals("Magang")){
                gaji=6000;
            }
            Karyawan kry = new Karyawan();
            kry.setLamaKerja(Double.parseDouble(data[1].toString()));
            kry.setGaji(gaji);
            karyawanControl.insertGajiKaryawan(kry.hitungGaji(), Karyawan.user, Double.parseDouble(data[1].toString()));
        }
        new LoginForm().setVisible(true);
        dispose();
    }//GEN-LAST:event_btnLogOutActionPerformed

    private void btnCokeActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnCokeActionPerformed
        // TODO add your handling code here:
        txtTotal.setText("");
        String kodeBarang="CC-1";
        int kondisi = cariBarangTransaksi(kodeBarang);
        if(kondisi==-1){
            updateTblTransaksi(3, 1);
        }else{
            int nilai = Integer.parseInt(tblTransaksi.getValueAt(kondisi, 2).toString())+1;
            tblModelTransaksi.removeRow(kondisi);
            updateTblTransaksi(3, nilai);
        }
    }//GEN-LAST:event_btnCokeActionPerformed

    private void btnPaket1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnPaket1ActionPerformed
        // TODO add your handling code here:
        btnBurgerActionPerformed(evt);
        btnCokeActionPerformed(evt);
    }//GEN-LAST:event_btnPaket1ActionPerformed

    private void btnAyamGorengActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnAyamGorengActionPerformed
        // TODO add your handling code here:
        String kodeBarang="FC-1";
        int kondisi = cariBarangTransaksi(kodeBarang);
        if(kondisi==-1){
            updateTblTransaksi(2, 1);
        }else{
            int nilai = Integer.parseInt(tblTransaksi.getValueAt(kondisi, 2).toString())+1;
            tblModelTransaksi.removeRow(kondisi);
            updateTblTransaksi(2, nilai);
        }
    }//GEN-LAST:event_btnAyamGorengActionPerformed

    private void btnFrenchFriesActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnFrenchFriesActionPerformed
        // TODO add your handling code here:
        txtTotal.setText("");
        String kodeBarang="FF-1";
        int kondisi = cariBarangTransaksi(kodeBarang);
        if(kondisi==-1){
            updateTblTransaksi(1, 1);
        }else{
            int nilai = Integer.parseInt(tblTransaksi.getValueAt(kondisi, 2).toString())+1;
            tblModelTransaksi.removeRow(kondisi);
            updateTblTransaksi(1, nilai);
        }
    }//GEN-LAST:event_btnFrenchFriesActionPerformed

    private void btnPepsiActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnPepsiActionPerformed
        // TODO add your handling code here:
        txtTotal.setText("");
        txtTotal.setText("");
        String kodeBarang="PS-1";
        int kondisi = cariBarangTransaksi(kodeBarang);
        if(kondisi==-1){
            updateTblTransaksi(4, 1);
        }else{
            int nilai = Integer.parseInt(tblTransaksi.getValueAt(kondisi, 2).toString())+1;
            tblModelTransaksi.removeRow(kondisi);
            updateTblTransaksi(4, nilai);
        }
    }//GEN-LAST:event_btnPepsiActionPerformed

    private void btnMangoFloatActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnMangoFloatActionPerformed
        // TODO add your handling code here:
        txtTotal.setText("");
        String kodeBarang="MF-1";
        int kondisi = cariBarangTransaksi(kodeBarang);
        if(kondisi==-1){
            updateTblTransaksi(5, 1);
        }else{
            int nilai = Integer.parseInt(tblTransaksi.getValueAt(kondisi, 2).toString())+1;
            tblModelTransaksi.removeRow(kondisi);
            updateTblTransaksi(5, nilai);
        }
    }//GEN-LAST:event_btnMangoFloatActionPerformed

    private void btnPaket2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnPaket2ActionPerformed
        // TODO add your handling code here:
        btnFrenchFriesActionPerformed(evt);
        btnPepsiActionPerformed(evt);
    }//GEN-LAST:event_btnPaket2ActionPerformed

    private void btnHapusActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnHapusActionPerformed
        // TODO add your handling code here:
        try{
            int rowIndex = tblTransaksi.getSelectedRow();
            int balik=Integer.parseInt(tblTransaksi.getValueAt(rowIndex, 2).toString());
            String kodeBarang=tblTransaksi.getValueAt(rowIndex, 1).toString();
            int indexBarang = cariBarang(kodeBarang);
            int barang = Integer.parseInt(tblBarang.getValueAt(indexBarang, 2).toString());
            tblModelTransaksi.removeRow(rowIndex);
            barangControl.restokBarang(balik+barang, kodeBarang);
            showTable();
        }catch(ArrayIndexOutOfBoundsException e){
            JOptionPane.showMessageDialog(this, "Tidak ada transaksi yang dipilih!", "Error", JOptionPane.ERROR_MESSAGE);
        }
    }//GEN-LAST:event_btnHapusActionPerformed

    private void btnBatalActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnBatalActionPerformed
        // TODO add your handling code here:
        int rowIndex = tblTransaksi.getRowCount();
        for(int i=0; i<rowIndex; i++){
            int balik=Integer.parseInt(tblTransaksi.getValueAt(i, 2).toString());
            String kodeBarang=tblTransaksi.getValueAt(i, 1).toString();
            int indexBarang = cariBarang(kodeBarang);
            int barang = Integer.parseInt(tblBarang.getValueAt(indexBarang, 2).toString());
            barangControl.restokBarang(balik+barang, kodeBarang);
        }
        tblModelTransaksi.getDataVector().removeAllElements();
        showTable();
        tblModelTransaksi.fireTableDataChanged();
        txtTotal.setText("");
        btnGroupPemesanan.clearSelection();
    }//GEN-LAST:event_btnBatalActionPerformed

    private void btnProsesActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnProsesActionPerformed
        // TODO add your handling code here:
        try{
            transaksiKosongException();
            jenisPemesananException(jrButtonPemesanan.isSelected() || jrButtonDelivery.isSelected());
            int jumlahBarang = hitungJumlahBarang();
            double jumlahHarga = jumlahHarga();
            txtTotal.setText("Total Pembelian : Rp " + jumlahHarga);
            String statusBarang;
            if(checkStokBarang()){
                statusBarang="Tunggu";
            }else{
                statusBarang="Tersedia";
            }
            if(jrButtonPemesanan.isSelected()){
                Pemesanan pemesanan = new Pemesanan();
                pemesanan.setJumlahItem(jumlahBarang);
                pemesanan.setTotal(jumlahHarga);
                pemesanan.setStatusBarang(statusBarang);
                pemesananControl.insertPemesanan(pemesanan);
                if(statusBarang.equals("Tunggu")){
                    new PemesananView().setVisible(true);
                }
            }else if(jrButtonDelivery.isSelected()){
                Delivery delivery = new Delivery();
                delivery.setJumlahItem(jumlahBarang);
                delivery.setSubTotal(jumlahHarga);
                deliveryControl.insertDelivery(delivery);
                new DeliveryView().setVisible(true);
            }
            tblModelTransaksi.getDataVector().removeAllElements();
            tblModelTransaksi.fireTableDataChanged();
            btnGroupPemesanan.clearSelection();
        }catch(TransaksiKosongException e){
            JOptionPane.showMessageDialog(this, e.message(), "Error", JOptionPane.ERROR_MESSAGE);
        }catch(JenisPemesananException e1){
            JOptionPane.showMessageDialog(this, e1.message(), "Error", JOptionPane.ERROR_MESSAGE);
        }
    }//GEN-LAST:event_btnProsesActionPerformed

    private void txtTotalActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtTotalActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtTotalActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(MainView.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(MainView.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(MainView.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(MainView.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new MainView().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnAyamGoreng;
    private javax.swing.JButton btnBatal;
    private javax.swing.JButton btnBurger;
    private javax.swing.JButton btnCoke;
    private javax.swing.JButton btnFrenchFries;
    private javax.swing.ButtonGroup btnGroupPegawai;
    private javax.swing.ButtonGroup btnGroupPemesanan;
    private javax.swing.JButton btnHapus;
    private javax.swing.JButton btnLogOut;
    private javax.swing.JButton btnMangoFloat;
    private javax.swing.JButton btnPaket1;
    private javax.swing.JButton btnPaket2;
    private javax.swing.JButton btnPepsi;
    private javax.swing.JButton btnProses;
    private javax.swing.JButton btnRestok;
    private javax.swing.JButton btnSimpan;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JScrollPane jScrollPane4;
    private javax.swing.JRadioButton jrButtonDelivery;
    private javax.swing.JRadioButton jrButtonMagang;
    private javax.swing.JRadioButton jrButtonPemesanan;
    private javax.swing.JRadioButton jrButtonPgwTetap;
    private javax.swing.JPanel panelPegawai;
    private javax.swing.JPasswordField pwdPassword;
    private javax.swing.JTabbedPane tabMenu;
    private javax.swing.JTable tblBarang;
    private javax.swing.JTable tblKaryawan;
    private javax.swing.JTable tblTransaksi;
    private javax.swing.JTextField txtNama;
    private javax.swing.JTextField txtTotal;
    // End of variables declaration//GEN-END:variables
}
