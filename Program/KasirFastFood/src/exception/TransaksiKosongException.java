/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package exception;

/**
 *
 * @author NatanHariPamungkas
 */
public class TransaksiKosongException extends Exception {
    public String message(){
        return "Tidak ada transaksi yang akan diproses!";
    }
}
