/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package exception;

/**
 *
 * @author NatanHariPamungkas
 */
public class NamaFormatException extends Exception{
    public String message(){
        return "Nama tidak boleh: \n"
                + "1. Mengandung karakter selain huruf kecil.\n"
                + "2. Kosong.";
    }
}
