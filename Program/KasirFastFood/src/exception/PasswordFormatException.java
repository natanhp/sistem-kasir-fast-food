/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package exception;

/**
 *
 * @author NatanHariPamungkas
 */
public class PasswordFormatException extends Exception{
    public String message(){
        return "Password yang dimasukan harus: \n"
                + "1. Minimal 8 karakter.\n"
                + "2. Mengandung angka.\n"
                + "3. Mengandung huruf kecil dan huruf besar.\n"
                + "4. Mengandung karakter khusus.\n"
                + "5. Tidak boleh kosong/spasi.\n"
                + "6. Tidak boleh sama dengan nama";
    }
}
