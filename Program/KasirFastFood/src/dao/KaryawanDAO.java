/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;

import java.security.NoSuchAlgorithmException;
import model.Karyawan;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
/**
 *
 * @author NatanHariPamungkas
 */
public class KaryawanDAO {
  public static Connection CON;
  public static final String url="jdbc:ucanaccess://";
  public static final String path="D:\\Restorandb.mdb";
  
  public void makeConnection(){
      System.out.println("Opening database...");
      try{
          CON = DriverManager.getConnection(url+path);
          System.out.println("Success");
     }catch(Exception e){
          System.err.print("Error opening database...\n");
          System.err.print(e);
     }
  }
  
  public void closeConnection(){
      System.out.println("Closing database...");
      try{
          CON.close();
          System.out.println("Success");
      }catch(Exception e){
          System.err.print("Error closing database...\n");
          System.err.print(e);
      }
  }
  
  public void insert(Karyawan K, String password) throws NoSuchAlgorithmException{
      String sql = "INSERT INTO Karyawan (nama,status,gaji,lamaShift, password)" +
              "VALUES ('"+K.getNama()+"','"+K.getStatus()+"',"+K.getGaji()+","+K.getLamaKerja()+",'"+K.hashPassword(password)+"')";
      System.out.println("Adding Karyawan...");
      
      try{
          Statement statement = CON.createStatement();
          int result = statement.executeUpdate(sql);
          System.out.println("Added " + result + " Karyawan\n");
          statement.close();
      }catch(Exception e){
          System.err.print("Error adding karyawan...\n");
          System.err.print(e);
      }
  }
  
  public Karyawan checkPassword(String hash, String nama){
      String sql= "SELECT nama,password FROM Karyawan WHERE password='"+hash+"' AND nama='"+nama+"'";
      System.out.println("Checking password...");
      
      Karyawan k= new Karyawan();
      boolean status=false;
      try{
          Statement statement = CON.createStatement();
          ResultSet rs = statement.executeQuery(sql);
          if(rs!=null){
              if(rs.next()){
                k.setNama(rs.getString("nama"));
                k.setHash(rs.getString("password"));
              }else{
                k.setNama("");
                k.setHash("");
              }
          }
      }catch(Exception e){
          System.err.print("Error reading karyawan...\n");
          System.err.print(e);
      }
      return k;
  }
  
  public void editKaryawan(Karyawan k, String status, String hash, String nama){
      String sql="UPDATE Karyawan SET status='"+status+"',password='"+hash+"' WHERE nama='"+nama+"'";
      
      System.out.println("Editing karyawan...");
      
      try{
          Statement statement = CON.createStatement();
          int result = statement.executeUpdate(sql);
          System.out.println("Edit " + result + "Karyawan\n"+ nama);
          statement.close();
      }catch(Exception e){
          System.err.print("Error editing karyawan...\n");
          System.err.print(e);
      }
  }
  
  public void deleteKaryawan(String nama){
      String sql="DELETE FROM Karyawan WHERE nama='"+nama+"'";
      System.out.println("Deleting karyawan..");
      
      try{
          Statement statement = CON.createStatement();
          int result = statement.executeUpdate(sql);
          System.out.println("Delete "+result+" Karyawan\n");
          statement.close();
      }catch(Exception e){
          System.err.print("Error deleting karyawan...\n");
          System.err.print(e);
      }
  }
  
  public List<Karyawan> showKaryawan(){
      String sql = "SELECT * FROM Karyawan";
      System.out.println("Show Karyawan");
      
      List<Karyawan> list= new ArrayList<>();
      
      try{
            Statement statement = CON.createStatement();
            ResultSet rs = statement.executeQuery(sql);
            
            if(rs!=null){
                while(rs.next()){
                    Karyawan k = new Karyawan(rs.getString("nama"), rs.getString("status"), Float.parseFloat(rs.getString("lamaShift")), Float.parseFloat(rs.getString("gaji")));
                    list.add(k);
                }
            }
            rs.close();
            statement.close();
        }catch(Exception e){
            System.out.println("Error database");
            System.out.println(e);
        }
        return list;
  }
  
  public List<Karyawan> showKaryawanTertentu(String nama){
      String sql = "SELECT * FROM Karyawan WHERE nama='"+nama+"'";
      System.out.println("Show Karyawan");
      
      List<Karyawan> list= new ArrayList<>();
      
      try{
            Statement statement = CON.createStatement();
            ResultSet rs = statement.executeQuery(sql);
            
            if(rs!=null){
                while(rs.next()){
                    Karyawan k = new Karyawan(rs.getString("nama"), rs.getString("status"), Float.parseFloat(rs.getString("lamaShift")), Float.parseFloat(rs.getString("gaji")));
                    list.add(k);
                }
            }
            rs.close();
            statement.close();
        }catch(Exception e){
            System.out.println("Error database");
            System.out.println(e);
        }
        return list;
  }
  
  public void updateTimeShiftKaryawan(double time, String nama){
      String sql = "UPDATE Karyawan SET lamaShift= lamaShift + " + time + " WHERE nama='"+nama+"'";
      System.out.println("Update karyawan");
      try{
          Statement statement = CON.createStatement();
          int result = statement.executeUpdate(sql);
          System.out.println("Edit " + result + "Karyawan\n"+ nama);
          statement.close();
      }catch(Exception e){
          System.err.print("Error updating time karyawan...\n");
          System.err.print(e);
      }
  }
  
  public Object[] updateGajiKaryawan(String nama){
      String sql = "SELECT status, lamaShift FROM Karyawan WHERE nama='"+nama+"'";
      Object[] data = null;
      try{
          Statement statement = CON.createStatement();
          ResultSet rs = statement.executeQuery(sql);
           data = new Object[3];
          if(rs!=null){
              while(rs.next()){
                  data[0] = rs.getString("status");
                  data[1] = rs.getString("lamaShift");
              }
          }
          statement.close();
      }catch(Exception e){
          System.err.print("Error updating time karyawan...\n");
          System.err.print(e);
      }
      return data;
  }
  
  public void insertGajiKaryawan(double gaji, String nama, double lamaShift){
      String sql = "UPDATE Karyawan SET gaji="+gaji+" WHERE nama='"+nama+"' AND lamaShift="+lamaShift;
      System.out.println("Adding Gaji Karyawan...");
      
      try{
          Statement statement = CON.createStatement();
          int result = statement.executeUpdate(sql);
          System.out.println("Added " + result + " Karyawan\n");
          statement.close();
      }catch(Exception e){
          System.err.print("Error adding gaji karyawan...\n");
          System.err.print(e);
      }
  }
}
