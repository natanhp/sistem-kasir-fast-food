/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.Statement;
import model.Delivery;

/**
 *
 * @author NatanHariPamungkas
 */
public class DeliveryDAO {
  public static Connection CON;
  public static final String url="jdbc:ucanaccess://";
  public static final String path="D:\\Restorandb.mdb";
  
  public void makeConnection(){
      System.out.println("Opening database...");
      try{
          CON = DriverManager.getConnection(url+path);
          System.out.println("Success");
     }catch(Exception e){
          System.err.print("Error opening database...\n");
          System.err.print(e);
     }
  }
  
  public void closeConnection(){
      System.out.println("Closing database...");
      try{
          CON.close();
          System.out.println("Success");
      }catch(Exception e){
          System.err.print("Error closing database...\n");
          System.err.print(e);
      }
  }
  
  public void insert(Delivery delivery){
      String sql = "INSERT INTO Delivery (jumlahItem,totalBiaya)"+
              "VALUES("+delivery.getJumlahItem()+","+delivery.hitungSubTotal()+")";
      System.out.println("Adding delivery....");
      
      try{
          Statement statement = CON.createStatement();
          int result = statement.executeUpdate(sql);
          System.out.println("Added "+ result + "delivery\n");
          statement.close();
      }catch(Exception e){
          System.err.print("Error adding delivery...\n");
          System.err.print(e);
      }
  }
  
  public void updateDataOrang(String nama, String noHP, String alamat){
      String sql = "UPDATE Delivery SET namaPemesan='"+nama+"', nomorHP='"+noHP+"', alamat='"+alamat+"' WHERE ID = (SELECT TOP 1 ID FROM Delivery ORDER BY ID DESC)";
      System.out.println("Update data orang...");
      
      try{
          Statement statement = CON.createStatement();
          int result = statement.executeUpdate(sql);
          System.out.println("Update data orang");
          statement.close();
      }catch(Exception e){
          System.err.print("Error updating data orang...\n");
          System.err.print(e);
      }
  }
}
