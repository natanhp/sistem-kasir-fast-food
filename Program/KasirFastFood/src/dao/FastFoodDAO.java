/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;
import java.security.NoSuchAlgorithmException;
import model.Barang;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import model.Delivery;
import model.Karyawan;
import model.Pemesanan;
/**
 *
 * @author NatanHariPamungkas
 */
public class FastFoodDAO {
  public static Connection CON;
  public static final String url="jdbc:ucanaccess://";
  public static final String path="D:\\Restorandb.mdb";
  
  public void makeConnection(){
      System.out.println("Opening database...");
      try{
          CON = DriverManager.getConnection(url+path);
          System.out.println("Success");
     }catch(Exception e){
          System.err.print("Error opening database...\n");
          System.err.print(e);
     }
  }
  
  public void closeConnection(){
      System.out.println("Closing database...");
      try{
          CON.close();
          System.out.println("Success");
      }catch(Exception e){
          System.err.print("Error closing database...\n");
          System.err.print(e);
      }
  }
  
  // Untuk barang
  public void insert(Barang b){
      String sql = "INSERT INTO Karyawan (nama,stok,harga,kodeBarang)"+
              "VALUES('"+b.getNamaBarang()+"',"+b.getStok()+","+b.getKodeBarang()+",'"+b.getKodeBarang()+"')";
      System.out.println("Adding barang....");
      
      try{
          Statement statement = CON.createStatement();
          int result = statement.executeUpdate(sql);
          System.out.println("Added "+ result + "Karyawan\n");
          statement.close();
      }catch(Exception e){
          System.err.print("Error adding barang...\n");
          System.err.print(e);
      }
  }
  
  public void restokBarang(int stok, String kodeBarang){
      String sql = "UPDATE Barang SET stok="+stok+" WHERE kodeBarang='"+kodeBarang+"'";
      System.out.println("Restoking barang...");
      
      try{
          Statement statement = CON.createStatement();
          int result = statement.executeUpdate(sql);
          System.out.println("Restok barang");
          statement.close();
      }catch(Exception e){
          System.err.print("Error restoking barang...\n");
          System.err.print(e);
      }
  }
  
  public List<Barang> showBarang(){
      String sql = "SELECT * FROM Barang";
      System.out.println("Show Barang");
      
      List<Barang> list= new ArrayList<>();
      
      try{
            Statement statement = CON.createStatement();
            ResultSet rs = statement.executeQuery(sql);
            
            if(rs!=null){
                while(rs.next()){
                    Barang b = new Barang(rs.getString("nama"), rs.getString("kodeBarang"), Integer.parseInt(rs.getString("stok")), Float.parseFloat(rs.getString("harga")));
                    list.add(b);
                }
            }
            rs.close();
            statement.close();
        }catch(Exception e){
            System.out.println("Error database");
            System.out.println(e);
        }
        return list;
  }
  
  //Untuk delivery
  public void insert(Delivery delivery){
      String sql = "INSERT INTO Delivery (jumlahItem,totalBiaya)"+
              "VALUES("+delivery.getJumlahItem()+","+delivery.hitungSubTotal()+")";
      System.out.println("Adding delivery....");
      
      try{
          Statement statement = CON.createStatement();
          int result = statement.executeUpdate(sql);
          System.out.println("Added "+ result + "delivery\n");
          statement.close();
      }catch(Exception e){
          System.err.print("Error adding delivery...\n");
          System.err.print(e);
      }
  }
  
  public void updateDataOrang(String nama, String noHP, String alamat){
      String sql = "UPDATE Delivery SET namaPemesan='"+nama+"', nomorHP='"+noHP+"', alamat='"+alamat+"' WHERE ID = (SELECT TOP 1 ID FROM Delivery ORDER BY ID DESC)";
      System.out.println("Update data orang...");
      
      try{
          Statement statement = CON.createStatement();
          int result = statement.executeUpdate(sql);
          System.out.println("Update data orang");
          statement.close();
      }catch(Exception e){
          System.err.print("Error updating data orang...\n");
          System.err.print(e);
      }
  }
  
  //Untuk Karyawan
  public void insert(Karyawan K, String password) throws NoSuchAlgorithmException{
      String sql = "INSERT INTO Karyawan (nama,status,gaji,lamaShift, password)" +
              "VALUES ('"+K.getNama()+"','"+K.getStatus()+"',"+K.getGaji()+","+K.getLamaKerja()+",'"+K.hashPassword(password)+"')";
      System.out.println("Adding Karyawan...");
      
      try{
          Statement statement = CON.createStatement();
          int result = statement.executeUpdate(sql);
          System.out.println("Added " + result + " Karyawan\n");
          statement.close();
      }catch(Exception e){
          System.err.print("Error adding karyawan...\n");
          System.err.print(e);
      }
  }
  
  public Karyawan checkPassword(String hash, String nama){
      String sql= "SELECT nama,password FROM Karyawan WHERE password='"+hash+"' AND nama='"+nama+"'";
      System.out.println("Checking password...");
      
      Karyawan k= new Karyawan();
      boolean status=false;
      try{
          Statement statement = CON.createStatement();
          ResultSet rs = statement.executeQuery(sql);
          if(rs!=null){
              if(rs.next()){
                k.setNama(rs.getString("nama"));
                k.setHash(rs.getString("password"));
              }else{
                k.setNama("");
                k.setHash("");
              }
          }
      }catch(Exception e){
          System.err.print("Error reading karyawan...\n");
          System.err.print(e);
      }
      return k;
  }
  
  public void editKaryawan(Karyawan k, String status, String hash, String nama){
      String sql="UPDATE Karyawan SET status='"+status+"',password='"+hash+"' WHERE nama='"+nama+"'";
      
      System.out.println("Editing karyawan...");
      
      try{
          Statement statement = CON.createStatement();
          int result = statement.executeUpdate(sql);
          System.out.println("Edit " + result + "Karyawan\n"+ nama);
          statement.close();
      }catch(Exception e){
          System.err.print("Error editing karyawan...\n");
          System.err.print(e);
      }
  }
  
  public void deleteKaryawan(String nama){
      String sql="DELETE FROM Karyawan WHERE nama='"+nama+"'";
      System.out.println("Deleting karyawan..");
      
      try{
          Statement statement = CON.createStatement();
          int result = statement.executeUpdate(sql);
          System.out.println("Delete "+result+" Karyawan\n");
          statement.close();
      }catch(Exception e){
          System.err.print("Error deleting karyawan...\n");
          System.err.print(e);
      }
  }
  
  public List<Karyawan> showKaryawan(){
      String sql = "SELECT * FROM Karyawan";
      System.out.println("Show Karyawan");
      
      List<Karyawan> list= new ArrayList<>();
      
      try{
            Statement statement = CON.createStatement();
            ResultSet rs = statement.executeQuery(sql);
            
            if(rs!=null){
                while(rs.next()){
                    Karyawan k = new Karyawan(rs.getString("nama"), rs.getString("status"), Float.parseFloat(rs.getString("lamaShift")), Float.parseFloat(rs.getString("gaji")));
                    list.add(k);
                }
            }
            rs.close();
            statement.close();
        }catch(Exception e){
            System.out.println("Error database");
            System.out.println(e);
        }
        return list;
  }
  
  public List<Karyawan> showKaryawanTertentu(String nama){
      String sql = "SELECT * FROM Karyawan WHERE nama='"+nama+"'";
      System.out.println("Show Karyawan");
      
      List<Karyawan> list= new ArrayList<>();
      
      try{
            Statement statement = CON.createStatement();
            ResultSet rs = statement.executeQuery(sql);
            
            if(rs!=null){
                while(rs.next()){
                    Karyawan k = new Karyawan(rs.getString("nama"), rs.getString("status"), Float.parseFloat(rs.getString("lamaShift")), Float.parseFloat(rs.getString("gaji")));
                    list.add(k);
                }
            }
            rs.close();
            statement.close();
        }catch(Exception e){
            System.out.println("Error database");
            System.out.println(e);
        }
        return list;
  }
  
  public void updateTimeShiftKaryawan(double time, String nama){
      String sql = "UPDATE Karyawan SET lamaShift= lamaShift + " + time + " WHERE nama='"+nama+"'";
      System.out.println("Update karyawan");
      try{
          Statement statement = CON.createStatement();
          int result = statement.executeUpdate(sql);
          System.out.println("Edit " + result + "Karyawan\n"+ nama);
          statement.close();
      }catch(Exception e){
          System.err.print("Error updating time karyawan...\n");
          System.err.print(e);
      }
  }
  
  public Object[] updateGajiKaryawan(String nama){
      String sql = "SELECT status, lamaShift FROM Karyawan WHERE nama='"+nama+"'";
      Object[] data = null;
      try{
          Statement statement = CON.createStatement();
          ResultSet rs = statement.executeQuery(sql);
           data = new Object[3];
          if(rs!=null){
              while(rs.next()){
                  data[0] = rs.getString("status");
                  data[1] = rs.getString("lamaShift");
              }
          }
          statement.close();
      }catch(Exception e){
          System.err.print("Error updating time karyawan...\n");
          System.err.print(e);
      }
      return data;
  }
  
  public void insertGajiKaryawan(double gaji, String nama, double lamaShift){
      String sql = "UPDATE Karyawan SET gaji="+gaji+" WHERE nama='"+nama+"' AND lamaShift="+lamaShift;
      System.out.println("Adding Gaji Karyawan...");
      
      try{
          Statement statement = CON.createStatement();
          int result = statement.executeUpdate(sql);
          System.out.println("Added " + result + " Karyawan\n");
          statement.close();
      }catch(Exception e){
          System.err.print("Error adding gaji karyawan...\n");
          System.err.print(e);
      }
  }
  
  //Untuk Pemesanan
  public void insert(Pemesanan pemesanan){
      String sql = "INSERT INTO Pemesanan (jumlahItem,totalBiaya,statusBarang)"+
              "VALUES("+pemesanan.getJumlahItem()+","+pemesanan.hitungSubTotal()+",'"+pemesanan.getStatusBarang()+"')";
      System.out.println("Adding pemesanan....");
      
      try{
          Statement statement = CON.createStatement();
          int result = statement.executeUpdate(sql);
          System.out.println("Added "+ result + "pemesanan\n");
          statement.close();
      }catch(Exception e){
          System.err.print("Error adding pemesanan...\n");
          System.err.print(e);
      }
  }
  
  public void updateNomorMeja(int nomorMeja){
      String sql = "UPDATE Pemesanan SET nomorMeja="+nomorMeja+" WHERE ID= (SELECT ID FROM Pemesanan ORDER BY ID DESC LIMIT 1)";
      System.out.println("Update Nomor meja...");
      
      try{
          Statement statement = CON.createStatement();
          int result = statement.executeUpdate(sql);
          System.out.println(result);
          System.out.println("Update Nomor meja");
          statement.close();
      }catch(Exception e){
          System.err.print("Error updating nomor meja...\n");
          System.err.print(e);
      }
  }
}
