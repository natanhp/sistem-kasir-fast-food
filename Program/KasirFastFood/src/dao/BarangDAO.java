/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;
import model.Barang;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
/**
 *
 * @author NatanHariPamungkas
 */
public class BarangDAO {
  public static Connection CON;
  public static final String url="jdbc:ucanaccess://";
  public static final String path="D:\\Restorandb.mdb";
  
  public void makeConnection(){
      System.out.println("Opening database...");
      try{
          CON = DriverManager.getConnection(url+path);
          System.out.println("Success");
     }catch(Exception e){
          System.err.print("Error opening database...\n");
          System.err.print(e);
     }
  }
  
  public void closeConnection(){
      System.out.println("Closing database...");
      try{
          CON.close();
          System.out.println("Success");
      }catch(Exception e){
          System.err.print("Error closing database...\n");
          System.err.print(e);
      }
  }
  
  public void insert(Barang b){
      String sql = "INSERT INTO Karyawan (nama,stok,harga,kodeBarang)"+
              "VALUES('"+b.getNamaBarang()+"',"+b.getStok()+","+b.getKodeBarang()+",'"+b.getKodeBarang()+"')";
      System.out.println("Adding barang....");
      
      try{
          Statement statement = CON.createStatement();
          int result = statement.executeUpdate(sql);
          System.out.println("Added "+ result + "Karyawan\n");
          statement.close();
      }catch(Exception e){
          System.err.print("Error adding barang...\n");
          System.err.print(e);
      }
  }
  
  public void restokBarang(int stok, String kodeBarang){
      String sql = "UPDATE Barang SET stok="+stok+" WHERE kodeBarang='"+kodeBarang+"'";
      System.out.println("Restoking barang...");
      
      try{
          Statement statement = CON.createStatement();
          int result = statement.executeUpdate(sql);
          System.out.println("Restok barang");
          statement.close();
      }catch(Exception e){
          System.err.print("Error restoking barang...\n");
          System.err.print(e);
      }
  }
  
  public List<Barang> showBarang(){
      String sql = "SELECT * FROM Barang";
      System.out.println("Show Barang");
      
      List<Barang> list= new ArrayList<>();
      
      try{
            Statement statement = CON.createStatement();
            ResultSet rs = statement.executeQuery(sql);
            
            if(rs!=null){
                while(rs.next()){
                    Barang b = new Barang(rs.getString("nama"), rs.getString("kodeBarang"), Integer.parseInt(rs.getString("stok")), Float.parseFloat(rs.getString("harga")));
                    list.add(b);
                }
            }
            rs.close();
            statement.close();
        }catch(Exception e){
            System.out.println("Error database");
            System.out.println(e);
        }
        return list;
  }
}
