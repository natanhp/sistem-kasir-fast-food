/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.Statement;
import model.Pemesanan;

/**
 *
 * @author NatanHariPamungkas
 */
public class PemesananDAO {
  public static Connection CON;
  public static final String url="jdbc:ucanaccess://";
  public static final String path="D:\\Restorandb.mdb";
  
  public void makeConnection(){
      System.out.println("Opening database...");
      try{
          CON = DriverManager.getConnection(url+path);
          System.out.println("Success");
     }catch(Exception e){
          System.err.print("Error opening database...\n");
          System.err.print(e);
     }
  }
  
  public void closeConnection(){
      System.out.println("Closing database...");
      try{
          CON.close();
          System.out.println("Success");
      }catch(Exception e){
          System.err.print("Error closing database...\n");
          System.err.print(e);
      }
  }
  
  public void insert(Pemesanan pemesanan){
      String sql = "INSERT INTO Pemesanan (jumlahItem,totalBiaya,statusBarang)"+
              "VALUES("+pemesanan.getJumlahItem()+","+pemesanan.hitungSubTotal()+",'"+pemesanan.getStatusBarang()+"')";
      System.out.println("Adding pemesanan....");
      
      try{
          Statement statement = CON.createStatement();
          int result = statement.executeUpdate(sql);
          System.out.println("Added "+ result + "pemesanan\n");
          statement.close();
      }catch(Exception e){
          System.err.print("Error adding pemesanan...\n");
          System.err.print(e);
      }
  }
  
  public void updateNomorMeja(int nomorMeja){
      String sql = "UPDATE Pemesanan SET nomorMeja="+nomorMeja+" WHERE ID= (SELECT ID FROM Pemesanan ORDER BY ID DESC LIMIT 1)";
      System.out.println("Update Nomor meja...");
      
      try{
          Statement statement = CON.createStatement();
          int result = statement.executeUpdate(sql);
          System.out.println(result);
          System.out.println("Update Nomor meja");
          statement.close();
      }catch(Exception e){
          System.err.print("Error updating nomor meja...\n");
          System.err.print(e);
      }
  }
}
