/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package control;

import dao.FastFoodDAO;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.List;
import model.Karyawan;
/**
 *
 * @author NatanHariPamungkas
 */
public class KaryawanControl {
    FastFoodDAO kDAO = new FastFoodDAO();
    
    public boolean checkPassword(String nama, String password) throws NoSuchAlgorithmException{
        Karyawan k = new Karyawan();
        String hash = k.hashPassword(password);
        kDAO.makeConnection();
        k= kDAO.checkPassword(hash, nama);
        kDAO.closeConnection();
        if(k.getHash().equals(hash) && k.getNama().equals(nama)){
            return true;
        }else{
            return false;
        }
    }
    
    public void inputKaryawan(Karyawan k, String password) throws NoSuchAlgorithmException{
        kDAO.makeConnection();
        kDAO.insert(k, password);
        kDAO.closeConnection();
    }
    
    public List<Karyawan> showKaryawan(){
        kDAO.makeConnection();
        List<Karyawan> list = new ArrayList();
        list = kDAO.showKaryawan();
        kDAO.closeConnection();
        return list;
    }
    
    public List<Karyawan> showKaryawanTertentu(String nama){
        kDAO.makeConnection();
        List<Karyawan> list = new ArrayList();
        list = kDAO.showKaryawanTertentu(nama);
        kDAO.closeConnection();
        return list;
    }
    
    public void updateTimeShiftKaryawan(double time, String nama){
        kDAO.makeConnection();
        kDAO.updateTimeShiftKaryawan(time, nama);
        kDAO.closeConnection();
    }
    
    public Object[] updateGajiKaryawan(String nama){ 
        kDAO.makeConnection();
        Object[] data= kDAO.updateGajiKaryawan(nama);
        kDAO.closeConnection();
        return data;
    }
    
    public void insertGajiKaryawan(double gaji, String nama, double timeShift){
        kDAO.makeConnection();
        kDAO.insertGajiKaryawan(gaji, nama, timeShift);
        kDAO.closeConnection();
    }
}
