/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package control;
import dao.FastFoodDAO;
import java.util.ArrayList;
import java.util.List;
import java.util.Vector;
import model.Barang;
import model.Karyawan;

/**
 *
 * @author NatanHariPamungkas
 */
public class BarangControl {
    FastFoodDAO barangDAO = new FastFoodDAO();
    Karyawan kry;
    
    public List<Barang> loadBarang(){
        kry = new Karyawan();
        barangDAO.makeConnection();
        kry.addBarang(barangDAO.showBarang());
        barangDAO.closeConnection();
        return kry.getBarang();
    }
    
    public void restokBarang(int stok, String kodeBarang){
        kry = new Karyawan(kodeBarang, stok);
        barangDAO.makeConnection();
        barangDAO.restokBarang(kry.getStokBarang(), kry.getKodeBarang());
        barangDAO.closeConnection();
    }
}
