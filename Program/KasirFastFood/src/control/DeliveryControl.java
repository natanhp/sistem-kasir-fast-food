/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package control;

import dao.FastFoodDAO;
import model.Delivery;
import model.Karyawan;

/**
 *
 * @author NatanHariPamungkas
 */
public class DeliveryControl {
    FastFoodDAO deliveryDAO = new FastFoodDAO();
    
    public void insertDelivery(Delivery delivery){
        Karyawan kry = new Karyawan();
        kry.addDelivery(delivery);
        deliveryDAO.makeConnection();
        deliveryDAO.insert(kry.getDelivery());
        deliveryDAO.closeConnection();
    }
    
    public void updateDataOrang(String nama, String noHP, String alamat){
        deliveryDAO.makeConnection();
        deliveryDAO.updateDataOrang(nama, noHP, alamat);
        deliveryDAO.closeConnection();
    }
}
