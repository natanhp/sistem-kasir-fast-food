/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package control;

import dao.FastFoodDAO;
import model.Karyawan;
import model.Pemesanan;

/**
 *
 * @author NatanHariPamungkas
 */
public class PemesananControl {
    FastFoodDAO pemesananDAO = new FastFoodDAO();
    
    public void insertPemesanan(Pemesanan pemesanan){
        Karyawan kry = new Karyawan();
        kry.setPemesanan(pemesanan);
        pemesananDAO.makeConnection();
        pemesananDAO.insert(kry.getPemesanan());
        pemesananDAO.closeConnection();
    }
    
    public void updateNomorMeja(int nomorMeja){
        pemesananDAO.makeConnection();
        pemesananDAO.updateNomorMeja(nomorMeja);
        pemesananDAO.closeConnection();
    }
}
