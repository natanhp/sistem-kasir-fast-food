# Sistem Kasir Fast Food

Project ini dibuat untuk memenuhi tugas besar Pemrograman Berorientasi Objek <br><br>
## Daftar Bagian Tugas Besar
Bagian
--------------|
[Template Jadwal Tugas Besar](Jadwal/Copy%20of%20INF1833%20-%20PBO%20-%20template%20jadwal%20pengerjaan%20tugas%20besar.xls)|
[Template Spesifikasi Tugas Besar](Spesifikasi/INF1833%20-%20PBO%20-%20template%20spesifikasi%20tugas%20besar%20-%20r2.0.doc)|
[Database](Database/Restorandb.mdb)|
[Program](Program/KasirFastFood)|
[Diagram Kelas](Diagram%20Kelas/Diagram Kelas.vsdx)|


## Contributors: 
* @natanhp
* @agungwpg
* @mathiass
* @Resti123
* @kskkrenzz